# probadorDeTriacs

Circuito para probar triscs, diacs y diodos.

Tiene dos verisiones las cuales se encuentran en
* [kicadEsteban](kicadEsteban) y
* [kicadPavel](kicadPavel).

Se recomienda utilizar la segunda versión y ya que tiene un layout más 
amigable, sin embargo se les invita a conocer la primer versión, realizada por
un estudiante en su primera aproximación a KiCAD.
